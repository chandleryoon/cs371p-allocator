# CS371p: Object-Oriented Programming Allocator Repo

* Name: Chandler Yoon, Murray Lee

* EID: cy5673, mwl647

* GitLab ID: @chandleryoon, @murraylee

* HackerRank ID: @chandlerjyoon, @murraylee

* Git SHA: (most recent Git SHA, final change to your repo will be adding this value)

* GitLab Pipelines: https://gitlab.com/chandleryoon/cs371p-allocator/pipelines

* Estimated completion time: 15 hours

* Actual completion time: 14 hours

* Comments: N/A
