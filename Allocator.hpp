// -----------
// Allocator.h
// -----------

#ifndef Allocator_h
#define Allocator_h
// --------
// includes
// --------

#include <iostream> // cin, cout
#include <cmath>
#include <cstdio>
#include <vector>
#include <algorithm>
#include <sstream>
using namespace std;

#include <cassert>   // assert
#include <cstddef>   // ptrdiff_t, size_t
#include <new>       // bad_alloc, new
#include <stdexcept> // invalid_argument
#include <cmath>

// ---------
// Allocator
// ---------

const int sizeInt = 4;

template <typename T, std::size_t N>
class my_allocator {
    // -----------
    // operator ==
    // -----------

    friend bool operator == (const my_allocator&, const my_allocator&) {
        return false;}                                                   // this is correct

    // -----------
    // operator !=
    // -----------

    friend bool operator != (const my_allocator& lhs, const my_allocator& rhs) {
        return !(lhs == rhs);}

    public:
        // --------
        // typedefs
        // --------


        // type of pointer returned
        using      value_type = T;
        // size of each object (8 bytes fixed)
        using       size_type = std::size_t;
        using difference_type = std::ptrdiff_t;

        using       pointer   =       value_type*;
        using const_pointer   = const value_type*;

        using       reference =       value_type&;
        using const_reference = const value_type&;

    private:
        // ----
        // data
        // ----

        char a[N];

        // -----
        // valid
        // -----

        /**
         * O(1) in space
         * O(n) in time
         * Goes through the heap and checks the signatures at the beginning and end of each sentinel are set correctly
         */
        bool valid () const {
            // <your code>
            // <use iterators>
            const_iterator b = begin();
            const_iterator e = end();

            while (b != e) {
                int first_sent_val = *b;

                // Move the pointer to the second sentinel for this block
                const int* ptr = (&*b + (abs(*b)/sizeInt) + 1);
                int second_sent_val = *ptr;

                if(first_sent_val != second_sent_val) {
                    return false;
                }
                ++b;
            }
            return true;
        }

    public:
        // ---------------
        // iterator
        // over the blocks
        // ---------------

        class iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const iterator& lhs, const iterator& rhs) {
                return *lhs == *rhs;}                                           

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const iterator& lhs, const iterator& rhs) {
                return !(*lhs == *rhs);}

            private:
                // ----
                // data
                // ----

                int* _p;

            public:
                // -----------
                // constructor
                // -----------

                iterator (int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                int& operator * () const {
                    // returns value inside sentinel
                    //int& temp = *_p;
                    return *_p;}           

                // -----------
                // operator ++
                // -----------

                iterator& operator ++ () {
                    // returns point to next sentinel
                    _p += (abs(*_p)/sizeInt) + 2;
                    return *this;}

                // -----------
                // operator ++
                // -----------

                iterator operator ++ (int) {
                    iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                iterator& operator -- () {
                    _p -= (abs(*_p)/sizeInt) + 2;
                    return *this;}

                // -----------
                // operator --
                // -----------

                iterator operator -- (int) {
                    iterator x = *this;
                    --*this;
                    return x;}};

        // ---------------
        // const_iterator
        // over the blocks
        // ---------------

        class const_iterator {
            // -----------
            // operator ==
            // -----------

            friend bool operator == (const const_iterator& lhs, const const_iterator& rhs) {
                return *lhs == *rhs;}                                                       

            // -----------
            // operator !=
            // -----------

            friend bool operator != (const const_iterator& lhs, const const_iterator& rhs) {
                return !(lhs == rhs);}

            private:
                // ----
                // data
                // ----

                const int* _p;

            public:
                // -----------
                // constructor
                // -----------

                const_iterator (const int* p) {
                    _p = p;}

                // ----------
                // operator *
                // ----------

                const int& operator * () const {
                    //returns read only version of pointer since a const
                    const int& p = *_p;
                    return p;}                 

                // -----------
                // operator ++
                // -----------

                const_iterator& operator ++ () {
                    _p += (abs(*_p)/sizeInt) + 2;
                    return *this;}

                // -----------
                // operator ++
                // -----------

                const_iterator operator ++ (int) {
                    const_iterator x = *this;
                    ++*this;
                    return x;}

                // -----------
                // operator --
                // -----------

                const_iterator& operator -- () {
                    _p -= (abs(*_p)/sizeInt) + 2;
                    return *this;}

                // -----------
                // operator --
                // -----------

                const_iterator operator -- (int) {
                    const_iterator x = *this;
                    --*this;
                    return x;}};

        // -----------
        // constructor
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         * throw a bad_alloc exception, if N is less than sizeof(T) + (2 * sizeof(int))
         */
        my_allocator () {
            // <your code>
            reinterpret_cast<int*>(a)[0]         = N - (2 * sizeInt);
            reinterpret_cast<int*>(a)[(N/sizeInt) - 1] = N - (2 * sizeInt);
            assert(valid());}

        my_allocator             (const my_allocator&) = default;
        ~my_allocator            ()                    = default;
        my_allocator& operator = (const my_allocator&) = default;

        // --------
        // allocate
        // --------

        /**
         * O(1) in space
         * O(n) in time
         * after allocation there must be enough space left for a valid block
         * the smallest allowable block is sizeof(T) + (2 * sizeof(int))
         * choose the first block that fits
         * throw a bad_alloc exception, if n is invalid
         */
        pointer allocate (size_type n) {

            //size_type is 8
            if(n == 0) {
                return NULL;
            }

            iterator b = begin();
            iterator e = end();
            bool found = false;

            int alloc_size = n * (2 * sizeInt);

            pointer result;
            while (b != e && !found)
            {
                // temp ptr
                int* ptr = &*b;

                // check if sentinel is positive and has enough space
                if (*b > 0 && *b >= alloc_size)
                {
                    found = true;
                    // returned pointer
                    result = reinterpret_cast<pointer>(ptr + 1);
                    // total free sentinel size - allocated size
                    int remaining_size = *b - (alloc_size);
                    
                    if (remaining_size < 16) // 2 Signature + 1 block = 16
                    {
                        // allocate new block with extra space
                        // equal to entirety of spot chosen
                        *b = -1 * (*b);
                        ptr += abs(*b) / sizeInt + 1;
                        *ptr = *b;

                    }
                    else
                    {
                        // use part of spot chosen
                        // putting new allocated value inside
                        *ptr = -1 * alloc_size;
                        // add end new sentinel (8 * n / 4)
                        // moves by 4 bytes for ints
                        ptr += (1 + alloc_size / sizeInt);
                        *ptr = -1 * alloc_size;
                        // create new free space sentinels
                        ptr += 1;
                        int free_size = remaining_size - 8;
                        *ptr = free_size;
                        // move by 4 bytes because int*
                        ptr += 1 + free_size / sizeInt;
                        *ptr = free_size;
                    }
                }
                ++b;
            }



            if (!found)
            {
                throw bad_alloc();
            }

            assert(valid());
            return result;}             

        // --------- 
        // construct
        // ---------

        /**
         * O(1) in space
         * O(1) in time
         */
        void construct (pointer p, const_reference v) {
            new (p) T(v);                               // this is correct and exempt
            assert(valid());}                           // from the prohibition of new

        // ----------
        // deallocate
        // ----------

        /**
         * O(1) in space
         * O(1) in time
         * after deallocation adjacent free blocks must be coalesced
         * throw an invalid_argument exception, if p is invalid
         * <your documentation>
         */
        void deallocate (pointer p, size_type s) {
            assert(valid());

            // Make a pointer pointing to the current block's first sentinel.
            int* current_sent_ptr = reinterpret_cast<int*>(p)- 1;
            int* left_sent_ptr = current_sent_ptr;
            int* right_sent_ptr = current_sent_ptr + (abs(*current_sent_ptr)/sizeInt) + 1;

            int newSize = abs(*current_sent_ptr);

            int* b = &(*begin());
            int* e = &(*end());
            
            if(current_sent_ptr < b || current_sent_ptr > e){
                throw invalid_argument("Invalid pointer");
            }

            // Check left only if we aren't at the beginning of the first block.

            if(left_sent_ptr != b) {
                int left_adj_sent = *(left_sent_ptr -1);

                if(left_adj_sent > 0) {
                    left_sent_ptr -= (left_adj_sent/sizeInt) + 2;
                    newSize += left_adj_sent + 8;
                }
            }

            // Check right only if we aren't at the end of the last block.

            if(right_sent_ptr + 1 != e) { // Add another 1 because end() will be at the very end of the heap, not the last sentinel.
                int right_adj_sent = *(right_sent_ptr + 1);

                if(right_adj_sent > 0) {
                    right_sent_ptr += (right_adj_sent/sizeInt) + 2;
                    newSize += right_adj_sent + 8;
                }
            }

            *left_sent_ptr = newSize;
            *right_sent_ptr = newSize;


            assert(valid());
        }

        // -------
        // destroy
        // -------

        /**
         * O(1) in space
         * O(1) in time
         */
        void destroy (pointer p) {
            p->~T();               // this is correct
            assert(valid());}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        int& operator [] (int i) {
            return *reinterpret_cast<int*>(&a[i]);}

        // -----------
        // operator []
        // -----------

        /**
         * O(1) in space
         * O(1) in time
         */
        const int& operator [] (int i) const {
            return *reinterpret_cast<const int*>(&a[i]);}

        // -----
        // begin
        // -----

        /**
         * O(1) in space
         * O(1) in time
         */
        iterator begin () {
            return iterator(&(*this)[0]);}

        // -----
        // begin
        // -----

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator begin () const {
            return const_iterator(&(*this)[0]);}

        // ---
        // end
        // ---

        /**s
         * O(1) in space
         * O(1) in time
         */
        iterator end () {
            // TODO: Why can't we use (*this) to index into a. ([] operator grabs &a[i] for some reasons)
            return iterator(&reinterpret_cast<int*>(a)[N/sizeInt]);
        }

        // ---
        // end
        // ---

        /**
         * O(1) in space
         * O(1) in time
         */
        const_iterator end () const {
            return const_iterator(&reinterpret_cast<const int*>(a)[N/sizeInt]);
        }
            
};
#endif // Allocator_h
