// ----------------
// RunAllocator.c++
// ----------------

// --------
// includes
// --------

#include <iostream> // cin, cout

#include "Allocator.hpp"

template<typename Iter> 
void print_heap(Iter b, Iter e) {
    int count = 0;
    while (b != e) {
        
        if(count != 0) 
            cout << " ";

        ++count;
        cout << *b;
        ++b;
    }
}

void allocator_solve (vector<int> requests) {
    my_allocator<double, 1000> x;
    const my_allocator<double, 1000> x2;

    for (int n : requests)
    {
        //if allocate 
        if (n > 0)
        {
            double* a = x.allocate(n);
        }
        else
        {
            // set the index to which taken sentinel to deallocate
            int idx = abs(n) - 1;

            my_allocator<double,1000>::iterator b = x.begin();
            my_allocator<double,1000>::iterator e = x.end();

            bool found = false;
            int* result;

            // search through heap till you find the correct block to deallocate
            while (b != e && !found)
            {
                if (*b < 0)
                {
                    if (idx == 0)
                    {
                        found = true;
                        result = &*b;
                    }
                    else
                    {
                        --idx;
                    }
                }
                ++b;
            }
            assert(result);

            //set a pointer to the spot of the block that you are going to free
            double* ptr = reinterpret_cast<double*>(&*result + 1);
            x.deallocate(ptr, 1);
        }   
    }

    my_allocator<double,1000>::iterator b = x.begin();
    my_allocator<double,1000>::iterator e = x.end();

    print_heap(b, e);
}



// ----
// main
// ----

int main () {
    using namespace std;
    
    int numTestCases;
    cin >> numTestCases;

    string blankLine;
    getline(cin, blankLine);
    getline(cin, blankLine);

    // loops over input for the amount of test cases
    for (int i = 0; i < numTestCases; ++i)
    {
        vector<int> requests;

        string line;
        while(getline(cin, line))
        {
            if (line == "")
            {
                break;
            }
            istringstream iss(line);
            int n;
            iss >> n;
            requests.push_back(n);
        }
        // saves the request in to a vector and solve solves this vector
        allocator_solve(requests);
        if(i != numTestCases - 1) {
            cout << endl;
        }
    }

    return 0;
}
